import { navigateHomepage } from "./navigateHomepage.js";
import { addToCart } from "./addToCart.js";
import { navigateToCart } from "./navigateToCart.js";
import { navigateToCheckout } from "./navigateToCheckout.js";
import { updateAddress } from "./updateAddress.js";
import { submitCheckout } from "./submitCheckout.js";

export const options = {
  thresholds: {
    http_req_duration: ['p(95)<4000'],
  },
  stages: [
    { target: 40, duration: '2m' },
    { target: 40, duration: '2m' },
    { target: 0, duration: '1m' },
  ]
};

// used to store global variables
const vars = [];

// global min/max sleep durations (in seconds):
const pauseMin = 5;
const pauseMax = 15;

export default function main() {
  navigateHomepage();
  addToCart();
  navigateToCart();
  navigateToCheckout();
  updateAddress();
  submitCheckout();
}
